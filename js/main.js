/*global $ */
/*eslint-env browser*/
$(document).ready(function () {
    'use strict';
    $('.register.login').click(function(){
      $('.drop-menu').toggleClass('active');
    });
    if($(window).width() <= 991){
      
      $('#navbar-collapse').removeClass('navbar');
  }
   
  $('.copied').slideUp(0);
  if($(window).width() >= 767){
    var x = $('.getHeight').height();
    $('.setHeight').height(x);
  }
    

    $('.service .slider').slick({
        rtl:true,
        slidesToShow: 8,
        slidesToScroll: 8,
        arrows:false,
        responsive: [
            {
              breakpoint: 1700,
              settings: {
                slidesToShow: 7,
                slidesToScroll: 7,
              }
            },
            {
              breakpoint: 1440,
              settings: {
                slidesToShow: 6,
                slidesToScroll: 6,
              }
            }
            ,
            {
              breakpoint: 1240,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 5,
              }
            },
            {
              breakpoint: 1084,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
              }
            },
            {
              breakpoint: 876,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              }
            },
            {
              breakpoint: 544,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              }
            }
            ,
            {
              breakpoint: 385,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode:true
              }
            }
           
          ]
    });

    
});
/* Add active To aide nav
======================== */
$(document).ready(function () {
  "use strict";
  $("#navbar-collapse li a").each(function () {
          var t = window.location.href.split(/[?#]/)[0];
          this.href == t && ($(this).parent().addClass("active"));
      })
});

/* share */
function copyToClipboard(element) {
    
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    $('.copied').slideDown();
  }